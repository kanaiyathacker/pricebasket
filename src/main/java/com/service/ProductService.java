package com.service;

import java.util.List;

import com.bean.Product;
import com.repository.PriceBucketRepository;

public class ProductService {
	
	public static ProductService productService;
	
	private ProductService() {}
	
	
	public static ProductService getInstance() {
		if(productService == null) {
			productService = new ProductService();
		}
		return productService;
	}


	public List<Product> getProductDetails(String[] products) {
		return PriceBucketRepository.getInstance().getProductDetails(products);
	}
	

}
