package com.service;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.bean.Product;
import com.bean.ProductDiscount;

public class PriceBucketService {
	
	public static PriceBucketService priceBucketService;
	
	private PriceBucketService() {}
	
	
	public static PriceBucketService getInstance() {
		if(priceBucketService == null) {
			priceBucketService = new PriceBucketService();
		}
		return priceBucketService;
	}


	public void calculatePriceBucket(String[] products) {
		List<Product> productDetails = ProductService.getInstance().getProductDetails(products);
		if(productDetails == null || productDetails.isEmpty()) {
			System.out.println("Invalid Product " + Arrays.toString(products));
		} else {
			List<ProductDiscount> productDiscountDetails = ProductDiscountService.getInstance().getProductDiscountDetails(productDetails);
			BigDecimal subTotal = calculateSubTotal(productDetails);
			System.out.println("SubTotal = �" + subTotal);
			BigDecimal totalDiscountAmount = applyDiscount(productDiscountDetails , productDetails);
			System.out.println("Total = �" + calculateTotal(subTotal, totalDiscountAmount));
		}
	}


	protected BigDecimal calculateTotal(BigDecimal subTotal, BigDecimal totalDiscountAmount) {
		return subTotal.subtract(totalDiscountAmount);
	}


	protected BigDecimal calculateSubTotal(List<Product> productDetails) {
		BigDecimal subTotal = productDetails.stream().map(p -> p.getPrice()).reduce(BigDecimal.ZERO, BigDecimal::add);
		return subTotal;
	}
	

	private BigDecimal applyDiscount(List<ProductDiscount> productDiscountDetails , List<Product> productDetails) {
		BigDecimal retVal = BigDecimal.ZERO; 
		if(productDiscountDetails != null && !productDiscountDetails.isEmpty() && productDetails != null && !productDetails.isEmpty()) {
			for(ProductDiscount pd : productDiscountDetails) {
				ProductDiscount rpd = pd.getRelatedProductDiscount();
				BigDecimal discountAmount = pd.getDiscountAmount();
				if(discountAmount != null && discountAmount.compareTo(BigDecimal.ZERO) > 0 || rpd != null) {
					long totalProduct = productDetails.stream().filter(p -> p.getName().equals(pd.getName())).count();
					int quantityRequired = pd.getQuantityRequired();
					if(discountAmount.compareTo(BigDecimal.ZERO) > 0 ) {
						BigDecimal calculatedDiscountAmount = applyProductDiscount(productDetails, pd , totalProduct);
						retVal = retVal.add(calculatedDiscountAmount);
					}
					if(rpd != null) {
						quantityRequired = (int) (quantityRequired == 0 ? totalProduct : (totalProduct/quantityRequired));
						BigDecimal calculatedDiscountAmount = applyProductDiscount(productDetails, rpd ,quantityRequired);
						retVal = retVal.add(calculatedDiscountAmount);
					}
				}
			}
		}
		if (retVal.compareTo(BigDecimal.ZERO) == 0) 
			System.out.println("(no offers available)");
		
		return retVal;
	}


	protected BigDecimal applyProductDiscount(List<Product> productDetails, ProductDiscount productDiscount, long totalDiscountApplied) {
		BigDecimal retVal = BigDecimal.valueOf(0.00).setScale(2);
		if(productDiscount != null) {
				String productName = productDiscount.getName();
				List<Product> itemsToDiscount = productDetails.stream()
						.filter(item -> item.getName().equals(productName))
						.limit(totalDiscountApplied).collect(Collectors.toList());
				
				for(Product p : itemsToDiscount) {
					BigDecimal discount = p.getPrice().multiply(productDiscount.getDiscountAmount()).divide(BigDecimal.valueOf(100));
					retVal = retVal.add(discount);
					System.out.println(productDiscount.getDiscountDesc( discount));
				}
		}
		return retVal;
	}
}
