package com.service;

import java.util.List;

import com.bean.Product;
import com.bean.ProductDiscount;
import com.repository.ProductDiscountRepository;

public class ProductDiscountService {
	
	public static ProductDiscountService productDiscountService;
	
	private ProductDiscountService() {}

	public static ProductDiscountService getInstance() {
		if(productDiscountService == null) {
			productDiscountService = new ProductDiscountService();
		}
		return productDiscountService;
	}

	public List<ProductDiscount> getProductDiscountDetails(List<Product> productDetails) {
		return ProductDiscountRepository.getInstance().getProductDiscountDetails(productDetails);
	}
}
