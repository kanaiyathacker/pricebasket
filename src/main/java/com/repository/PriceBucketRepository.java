package com.repository;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.bean.Product;

public class PriceBucketRepository {
	
public static PriceBucketRepository priceBucketRepository;

	Map<String , Product> productDetails;
	
	private PriceBucketRepository() {
		productDetails = buildProductDetails();
	}
	
	
	private Map<String, Product> buildProductDetails() {
		Map<String, Product> retVal = new HashMap<String, Product>();
		retVal.put("Soup", new Product("Soup", new BigDecimal("0.65")));
		retVal.put("Bread", new Product("Bread", new BigDecimal("0.90")));
		retVal.put("Milk", new Product("Milk", new BigDecimal("1.30")));
		retVal.put("Apples", new Product("Apples", new BigDecimal("1.00")));
		return retVal;
	}


	public static PriceBucketRepository getInstance() {
		if(priceBucketRepository == null) {
			priceBucketRepository = new PriceBucketRepository();
		}
		return priceBucketRepository;
	}


	public List<Product> getProductDetails(String[] products) {
		return Arrays.stream(products)
			   .map(val -> productDetails.get(val)).filter(val -> val != null).collect(Collectors.toList());
	}

}
