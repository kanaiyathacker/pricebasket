package com.repository;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.bean.Product;
import com.bean.ProductDiscount;

public class ProductDiscountRepository {
	
	public static ProductDiscountRepository productDiscountRepository;
	
	Map<String , ProductDiscount> productDiscount;
	
	private ProductDiscountRepository() {
		productDiscount = buildProductDiscount();
	}
	
	private Map<String, ProductDiscount> buildProductDiscount() {
		Map<String, ProductDiscount> retVal = new HashMap<String, ProductDiscount>();
		retVal.put("Apples", new ProductDiscount("Apples", "PERCENTAGE" , new BigDecimal("10") , 0));
		retVal.put("Soup", new ProductDiscount("Soup", null , new BigDecimal("0")  , 2 , new ProductDiscount("Bread", "PERCENTAGE" , new BigDecimal("50") , 0)));
		return retVal;
	}


	public static ProductDiscountRepository getInstance() {
		if(productDiscountRepository == null) {
			productDiscountRepository = new ProductDiscountRepository();
		}
		return productDiscountRepository;
	}

	public List<ProductDiscount> getProductDiscountDetails(List<Product> productDetails) {
		return productDetails.stream()
				   .map(val -> productDiscount.get(val.getName())).filter(val -> val != null).distinct().collect(Collectors.toList());
	}
}
