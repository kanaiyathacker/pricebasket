package com.bean;

import java.math.BigDecimal;

import com.util.Util;

public class ProductDiscount {
	private String name;
	private String discountType;
	private int quantityRequired;
	private BigDecimal discountAmount;
	private ProductDiscount relatedProductDiscount;
	
	public ProductDiscount(String name, String discountType, BigDecimal discountAmount , int quantityRequired) {
		this.name = name;
		this.discountType = discountType;
		this.discountAmount = discountAmount;
		this.quantityRequired = quantityRequired;
	}
	public ProductDiscount(String name, String discountType , BigDecimal discountAmount , int quantityRequired , ProductDiscount relatedProductDiscount) {
		this.name = name;
		this.discountType = discountType;
		this.discountAmount = discountAmount;
		this.quantityRequired = quantityRequired;
		this.relatedProductDiscount = relatedProductDiscount;
	}
	public ProductDiscount getRelatedProductDiscount() {
		return relatedProductDiscount;
	}
	public void setRelatedProductDiscount(ProductDiscount relatedProductDiscount) {
		this.relatedProductDiscount = relatedProductDiscount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDiscountType() {
		return discountType;
	}
	public void setDiscountType(String discountType) {
		this.discountType = discountType;
	}
	public BigDecimal getDiscountAmount() {
		return discountAmount;
	}
	public void setDiscountAmount(BigDecimal discountAmount) {
		this.discountAmount = discountAmount;
	}
	public int getQuantityRequired() {
		return quantityRequired;
	}
	public void setQuantityRequired(int quantityRequired) {
		this.quantityRequired = quantityRequired;
	}
	public String getDiscountDesc(BigDecimal amount) {
		String retVal =  "";
		if(amount != null && amount.compareTo(BigDecimal.ZERO) > 0) {
			retVal =  name + " " + discountAmount + "% Off: -" + Util.formatAmount(amount);
		}
		return retVal;
	}
}
