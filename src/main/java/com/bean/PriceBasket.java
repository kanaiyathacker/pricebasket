package com.bean;

import java.math.BigDecimal;
import java.util.List;

public class PriceBasket {

	private BigDecimal subTotal;
	private List<String> discountDescription;
	private BigDecimal total;
	
	public PriceBasket(BigDecimal subTotal, List<String> discountDescription, BigDecimal total) {
		this.subTotal = subTotal;
		this.discountDescription = discountDescription;
		this.total = total;
	}
	public BigDecimal getSubTotal() {
		return subTotal;
	}
	public void setSubTotal(BigDecimal subTotal) {
		this.subTotal = subTotal;
	}
	public List<String> getDiscountDescription() {
		return discountDescription;
	}
	public void setDiscountDescription(List<String> discountDescription) {
		this.discountDescription = discountDescription;
	}
	public BigDecimal getTotal() {
		return total;
	}
	public void setTotal(BigDecimal total) {
		this.total = total;
	}
}
