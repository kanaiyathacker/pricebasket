package com.pricebasket;
import com.service.PriceBucketService;

public class PriceBasket {
	
	public static void main(String[] products) {
		if(products.length == 0) {
			System.out.println("********** Please enter the product name **********");
			System.exit(0);
		}
		PriceBucketService.getInstance().calculatePriceBucket(products);
	}
}
