package com.util;

import java.math.BigDecimal;

public class Util {

	public static String formatAmount(BigDecimal value) {
		String retVal = "";
		if(value != null) {
			if(value.compareTo(BigDecimal.valueOf(1)) < 0 ) {
				retVal = value.multiply(BigDecimal.valueOf(100)).toBigInteger() + "p";
			} else {
				retVal = "�"+value.setScale(2);
			}
		}
		return retVal;
	}
}
