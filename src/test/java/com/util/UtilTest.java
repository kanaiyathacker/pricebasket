package com.util;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.junit.Test;

public class UtilTest {

	
	@Test
	public void testFormatAmountWhenValueIsNull() {
		String formatAmount = Util.formatAmount(null);
		assertEquals("", formatAmount);
	}
	
	@Test
	public void testFormatAmountWhenValueIsLessThanOne() {
		String formatAmount = Util.formatAmount(BigDecimal.valueOf(0.10));
		assertEquals("10p", formatAmount);
	}
	
	@Test
	public void testFormatAmountWhenValueIsGreaterThanOne() {
		String formatAmount = Util.formatAmount(BigDecimal.valueOf(1.20));
		assertEquals("�1.20", formatAmount);
	}
	
}
