package com.service;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bean.Product;
import com.bean.ProductDiscount;

public class PriceBucketServiceTest {
	
	private PriceBucketService priceBucketServiceTest;
	
	@Before
	public void before() {
		priceBucketServiceTest = PriceBucketService.getInstance(); 
	}
	
	@Test
	public void testApplyProductDiscount() {
		List<Product> productDetails = new ArrayList<>();
		Product product = new Product("Bread", BigDecimal.valueOf(0.90));
		productDetails.add(product );
		ProductDiscount productDiscount =  new ProductDiscount("Bread", "PERCENTAGE", BigDecimal.valueOf(50) , 0);
		long discountAppliedToTotalRelatedProduct = 1;
		BigDecimal applyRelatedProductDiscount = priceBucketServiceTest.applyProductDiscount(productDetails, productDiscount, discountAppliedToTotalRelatedProduct);
		assertEquals(BigDecimal.valueOf(0.45), applyRelatedProductDiscount);
	}
	
	@Test
	public void testApplyProductDiscountDiscountNotApplied() {
		List<Product> productDetails = new ArrayList<>();
		Product product = new Product("Bread", BigDecimal.valueOf(0.90));
		productDetails.add(product );
		ProductDiscount productDiscount =  new ProductDiscount("Bread", "PERCENTAGE", BigDecimal.valueOf(50) , 0);
		long discountAppliedToTotalRelatedProduct = 0;
		BigDecimal applyRelatedProductDiscount = priceBucketServiceTest.applyProductDiscount(productDetails, productDiscount, discountAppliedToTotalRelatedProduct);
		assertEquals(BigDecimal.valueOf(0.00).setScale(2), applyRelatedProductDiscount);
	}
	
	@Test
	public void testApplyDiscount() {
		List<Product> productDetails = new ArrayList<>();
		Product product = new Product("Apple", BigDecimal.valueOf(0.90));
		productDetails.add(product );
		product = new Product("Apple", BigDecimal.valueOf(0.90));
		productDetails.add(product );
		long totalDiscountApplied = 1;
		ProductDiscount productDiscount =  new ProductDiscount("Apple", "PERCENTAGE", BigDecimal.valueOf(50) , 0);
		BigDecimal applyProductDiscount = priceBucketServiceTest.applyProductDiscount(productDetails, productDiscount, totalDiscountApplied);
		assertEquals(BigDecimal.valueOf(0.45), applyProductDiscount);
	}
	
	@Test
	public void testApplyDiscountWhenDiscountNotAppliedToAllTheProducts() {
		List<Product> productDetails = new ArrayList<>();
		Product product = new Product("Bread", BigDecimal.valueOf(0.80));
		productDetails.add(product );
		product = new Product("Bread", BigDecimal.valueOf(0.80));
		productDetails.add(product );
		new Product("Bread", BigDecimal.valueOf(0.80));
		productDetails.add(product);
		long totalDiscountApplied = 1;
		ProductDiscount productDiscount =  new ProductDiscount("Bread", "PERCENTAGE", BigDecimal.valueOf(50) , 0 );
		BigDecimal applyProductDiscount = priceBucketServiceTest.applyProductDiscount(productDetails, productDiscount, totalDiscountApplied);
		assertEquals(BigDecimal.valueOf(0.40).setScale(2), applyProductDiscount);
	}
	
	@Test
	public void testCalculateSubTotal() {
		List<Product> productDetails = new ArrayList<>();
		Product product = new Product("Apples", BigDecimal.valueOf(1.00));
		productDetails.add(product );
		product = new Product("Milk", BigDecimal.valueOf(1.30));
		productDetails.add(product );
		new Product("Bread", BigDecimal.valueOf(0.80));
		productDetails.add(product);
		product = new Product("Soup", BigDecimal.valueOf(0.65));
		productDetails.add(product );
		BigDecimal calculateSubTotal = priceBucketServiceTest.calculateSubTotal(productDetails);
		assertEquals(BigDecimal.valueOf(4.25), calculateSubTotal);
	}
	
	@Test
	public void testCalculateTotal() {
		BigDecimal calculateTotal = priceBucketServiceTest.calculateTotal(BigDecimal.valueOf(4.25), BigDecimal.valueOf(0.90));
		assertEquals(BigDecimal.valueOf(3.35), calculateTotal);
	}
}
